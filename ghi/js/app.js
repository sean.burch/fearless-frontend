function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
        <div class="col-4">
            <div class="card shadow mx-1 mb-4">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                    <div class="card-footer">
            ${startDate} - ${endDate}
                    </div>
                </div>
            </div>
        </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
        console.log(response)

        if (!response.ok) {
            console.error("Can't fetch Conference list! Bad Request")

            const mainTag = document.querySelector('main')
            const errorHTML = `<div class="alert alert-warning" role="alert">
                            Can't fetch Conference List!
                            </div>`
            mainTag.innerHTML = errorHTML

        } else {
        const data = await response.json();
        console.log(data)

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            console.log(detailResponse)
            if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details)
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts).toLocaleDateString();
            const endDate = new Date(details.conference.ends).toLocaleDateString();
            const location = details.conference.location.name;

            const html = createCard(name, description, pictureUrl, startDate, endDate, location);
            const row = document.querySelector('.row');
            row.innerHTML += html;
            }
        }


        }
    } catch (error) {
        const mainTag = document.querySelector('main')
        const errorHTML = `<div class="alert alert-warning" role="alert">
                            Error!
                            </div>`
        mainTag.innerHTML = errorHTML
    }

});
